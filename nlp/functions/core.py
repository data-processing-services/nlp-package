from nltk import sent_tokenize
from nltk.tokenize import RegexpTokenizer
import re
import requests
import json

punctuation = ".’'()[]{}<>:,‒–—―…!.«»-‐?‘’“”„;/⁄␠·&@*\\•^¤¢$€£¥₩₪†‡°¡¿¬#№%‰‱¶′§~¨_|¦⁂☞∴‽※"
abbreviations = {
            "ca." : {"lang" : "de", "expan" : "cirka"},
            "etc." : {"lang" : "de", "expan" : "ect"},
            "z.B." : {"lang" : "de", "expan" : "zum Beispiel"}
        }

def cab_analytics( tokens ):
    tokens = json.dumps(tokens) if not isinstance(tokens, str) else tokens
    base_url = "http://www.deutschestextarchiv.de/demo/cab/query"
    #params = "fmt=raw&amp;ofmt=json&amp;a=expand"
    params = "fmt=json&amp;ofmt=json&amp;a=expand"
    r = requests.post( base_url+"?"+params, data=tokens )
    json_data = json.loads(r.text)
    tokens = json_data.get("body")[0].get("tokens")
    return tokens

def cab_normaliser( tokens ):
    analysis = cab_analytics( tokens )
    return [ token.get("moot").get("word") for token in analysis ] 

def cab_lemmatiser( tokens ):
    analysis = cab_analytics( tokens )
    return [ token.get("moot").get("lemma") for token in analysis ]

def join( token ):
    return " ".join(token)

def map( lst, mapping ):
    return [ mapping[ word ] if mapping.get(word) else word for word in lst ]

def normalise( lst, mapping ):
    return map( lst, mapping )

def removePunctuation ( data, punctuationCharacters = None ):
    pc = [c for c in punctuationCharacters] if punctuationCharacters else [c for c in punctuation]
    if isinstance(data, str):
        return "".join([char for char in data if char not in pc])
    elif isinstance(data, list):
        filtered = []
        for token in data:
            cleared = "".join([char for char in token if char not in pc])
            if cleared != "":
                filtered.append( cleared )
        return filtered

def removeStopwords( lst, stopwords = None ):
    sws = stopwords if stopwords else self.stopwords
    return [word for word in lst if word.lower() not in sws]

def removeWord( lst, blacklist ):
    return [word for word in lst if word.lower() not in blacklist]

def regex_cleaner( data, regex ):
    if isinstance(data, list):
        return [word for word in data if not re.match(regex, word.strip())]
    elif isinstance( data, str ):
        return re.sub(regex, "", data)
    else:
        return data

def replacePattern( data, regex, pattern ):
    #print(data)
    return [ re.sub( regex , pattern, word ) for word in data ]

def tokenizeRegEx( text, regex ):
    tokenizer = RegexpTokenizer( regex )
    return [ word.strip() for word in tokenizer.tokenize( text ) ]

def tokenizeAbbr( text, abbr = None, regex = '\w+|\$[\d\.]+|\S+'):
    abbr = abbr if abbr != None else abbreviations
    regex = "|".join( [ key.replace(".", "\.") for key in abbr] ) + "|" + regex
    return tokenizeRegEx( text, regex )

def tokenizerSentence( text, language='german' ):
        return sent_tokenize( text, language )

def transformCases( lst, mode = "lower"):
    if mode == "lower":
        return [ word.lower() for word in lst ]
    else:
        return [ word.upper() for word in lst ]
        