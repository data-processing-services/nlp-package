from setuptools import setup

setup(name='nlp',
      version='1.0.0',
      description='nlp functions',
      author='Uwe Sikora',
      author_email='sikora@sub.uni-goettingen.de',
      license='MIT',
      packages=['nlp', 'nlp.languages', 'nlp.functions'],
      install_requires=['nltk', 'requests'], #external packages as dependencies
      zip_safe=False)
