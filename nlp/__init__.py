#__all__ = ["functions", "languages"]
# import to handle directories as separate namespaces in the "nlp" namespace
from . import languages
from . import functions